package br.com.senaigoias.util;

import java.math.BigDecimal;

public class BigBugDecimal {
	
	public static void main(String[] args) {
		
		double v1 = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1;
		BigDecimal n = BigDecimal.valueOf(0.1);
		BigDecimal v2 = n.add(n).add(n).add(n).add(n).add(n).add(n).add(n);
		
		System.out.println(v1);
		System.out.println(v2);
		
	}

}
