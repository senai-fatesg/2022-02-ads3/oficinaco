package br.com.senaigoias.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import br.com.senaigoias.dao.ServicoDao;
import br.com.senaigoias.entidade.Servico;

public class ServicoTeste {
	
	public static void main(String[] args) {
		
		Connection con = Util.getConexao();
		ServicoDao servicoDao = new ServicoDao();
		List<Servico> servicos = servicoDao.listar(con);
		
		System.out.println("Listagem de servicos");
		for(Servico s : servicos) {
			System.out.println(s.getNome());
		}
		
		servicos.forEach(s -> {
			System.out.println(s.getNome());
			System.out.println(s.getId());
			});
		
		servicos.forEach(System.out::println);
		
		System.out.println("Fim da listagem\n\n");
		
		System.out.println("Teste insercao...");
		Servico servico = new Servico();
		servico.setNome("Teste insercao");
		servico.setPreco(BigDecimal.valueOf(10));
		servicoDao.incluir(con, servico);

		servicos = servicoDao.listar(con);
		
		System.out.println("Conferindo se o servico foi incluido");
		for(Servico s : servicos) {
			System.out.printf("%10d %s\n", s.getId(), s.getNome());
		}
		
		System.out.println("Teste alterar...n");
		Scanner scan = new Scanner(System.in);
		System.out.println("Digite o id para alterar: ");
		Integer id = scan.nextInt();
		scan.nextLine();
		System.out.println("Digite o novo nome do produto");
		String nome = scan.nextLine();
		System.out.println("Digite o novo preço do produto");
		double preco = scan.nextDouble();
		scan.nextLine();
		
		Servico srvAlt = new Servico();
		srvAlt.setId(id);
		srvAlt.setNome(nome);
		srvAlt.setPreco(BigDecimal.valueOf(preco));
		servicoDao.alterar(con, srvAlt);

		System.out.println("Conferindo se o servico foi alterado");
		servicos = servicoDao.listar(con);
		for(Servico s : servicos) {
			System.out.printf("%10d %s\n", s.getId(), s.getNome());
		}
		
		System.out.println("\n\nTeste exclusao");
		System.out.println("Digite o id para excluir: ");
		id = scan.nextInt();
		scan.nextLine();
		servicoDao.deletar(con, id);
		System.out.println("Conferindo se o servico foi excluido");
		servicos = servicoDao.listar(con);
		for(Servico s : servicos) {
			System.out.printf("%10d %s\n", s.getId(), s.getNome());
		}
		
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
