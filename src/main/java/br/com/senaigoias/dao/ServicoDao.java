package br.com.senaigoias.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.senaigoias.entidade.Servico;

public class ServicoDao {
	
	public List<Servico> listar(Connection con){
		List<Servico> resultado = new ArrayList<>();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from servico");
			while(rs.next()) {
				Servico s = new Servico();
				s.setId(rs.getInt("id"));
				s.setNome(rs.getString("nome"));
				s.setPreco(rs.getBigDecimal("preco"));
				resultado.add(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resultado;
	}
	
	public void incluir(Connection con, Servico servico) {
		try {
			PreparedStatement pstmt = con.prepareStatement("insert into servico (nome, preco) values (? , ?)");
			pstmt.setString(1, servico.getNome());
			pstmt.setBigDecimal(2, servico.getPreco());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void alterar(Connection con, Servico servico) {
		try {
			PreparedStatement pstmt = con.prepareStatement("update servico set nome = ?, preco = ? where id = ?");
			pstmt.setString(1, servico.getNome());
			pstmt.setBigDecimal(2, servico.getPreco());
			pstmt.setInt(3, servico.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deletar(Connection con, Integer id) {
		try {
			PreparedStatement pstmt = con.prepareStatement("delete from servico where id = ?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}






